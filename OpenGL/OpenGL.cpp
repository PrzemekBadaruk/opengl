﻿// OpenGL.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include <iostream>
#include "scenerunner.h"
#include "scenephong.h"

//texture
const char* awesomeFacePath = "img/awesomeface.png";
const char* containerPath = "img/container.jpg";

int main()
{
	SceneRunner sceneRunner("Scene Pong");
	std::unique_ptr<Scene> scene = std::unique_ptr<Scene>(new ScenePhong());
	return sceneRunner.run(std::move(scene));
}
