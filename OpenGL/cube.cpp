#include "cube.h"

Cube::Cube(GLfloat side) : TriangleMesh(), GameObject()
{
    GLfloat side2 = side / 2.0f;
    //default side  = 1.0, side2 = 0.5f
    std::vector<GLfloat> position = {
        // Front
     //-0.5f , -0.5f , 0.5f | 0.5f , -0.5f , 0.5f | 0.5f ,  0.5f , 0.5f |  -0.5f ,  0.5f , 0.5f
       -side2, -side2, side2, side2, -side2, side2, side2,  side2, side2,  -side2,  side2, side2,
       // Right
     // 0.5f , -0.5f , 0.5f | 0.5f , -0.5f , -0.5f | 0.5f ,  0.5f , -0.5f | 0.5f ,  0.5f , 0.5f
        side2, -side2, side2, side2, -side2, -side2, side2,  side2, -side2, side2,  side2, side2,
        // Back
     // -0.5f , -0.5f , -0.5f | -0.5f ,  0.5f , -0.5f | 0.5f ,  0.5f , -0.5f | 0.5f , -0.5f , -0.5f
        -side2, -side2, -side2, -side2,  side2, -side2, side2,  side2, -side2, side2, -side2, -side2,
        // Left
     // -0.5f , -0.5f , 0.5f | -0.5f ,  0.5f , 0.5f | -0.5f ,  0.5f , -0.5f | -0.5f , -0.5f , -0.5f
        -side2, -side2, side2, -side2,  side2, side2, -side2,  side2, -side2, -side2, -side2, -side2,
        // Bottom
     // -0.5f , -0.5f , 0.5f | -0.5f , -0.5f , -0.5f | 0.5f , -0.5f , -0.5f | 0.5f , -0.5f , 0.5f
        -side2, -side2, side2, -side2, -side2, -side2, side2, -side2, -side2, side2, -side2, side2,
        // Top
     // -0.5f ,  0.5f , 0.5f | 0.5f ,  0.5f , 0.5f | 0.5f ,  0.5f , -0.5f | -0.5f ,  0.5f , -0.5f
        -side2,  side2, side2, side2,  side2, side2, side2,  side2, -side2, -side2,  side2, -side2
    };
    //all normal vectors are perpendicular to side
    std::vector<GLfloat> normal = {
        // Front
        0.0f, 0.0f, 1.0f ,   0.0f, 0.0f, 1.0f ,  0.0f, 0.0f, 1.0f ,   0.0f, 0.0f, 1.0f,
        // Right
        1.0f, 0.0f, 0.0f ,   1.0f, 0.0f, 0.0f ,  1.0f, 0.0f, 0.0f ,   1.0f, 0.0f, 0.0f,
        // Back
        0.0f, 0.0f, -1.0f,   0.0f, 0.0f, -1.0f,  0.0f, 0.0f, -1.0f,   0.0f, 0.0f, -1.0f,
        // Left
        -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f ,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,
        // Bottom
        0.0f, -1.0f, 0.0f,   0.0f, -1.0f, 0.0f,  0.0f, -1.0f, 0.0f,   0.0f, -1.0f, 0.0f,
        // Top
        0.0f, 1.0f, 0.0f ,   0.0f, 1.0f, 0.0f ,  0.0f, 1.0f, 0.0f ,   0.0f, 1.0f, 0.0f
    };
    std::vector<GLfloat> textureCoord = {
        // Front
        0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        // Right
        0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        // Back
        0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        // Left
        0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        // Bottom
        0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
        // Top
        0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f
    };


    std::vector<GLuint> elements = {
        0,1,2,0,2,3,
        4,5,6,4,6,7,
        8,9,10,8,10,11,
        12,13,14,12,14,15,
        16,17,18,16,18,19,
        20,21,22,20,22,23
    };

    initBuffers(&elements, &position, &normal, &textureCoord);
}
