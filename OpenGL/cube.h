#ifndef _CUBE_H_
#define _CUBE_H_
#include "gameobject.h"
#include "drawable.h"
#include "trianglemesh.h"


class Cube : public TriangleMesh, public GameObject
{
public:
	Cube(GLfloat side = 1.0f);
};

#endif