#ifndef _DRAWABLE_H_
#define _DRAWABLE_H_

class Drawable
{
public:
	virtual void render() const = 0;
};

#endif // !1
