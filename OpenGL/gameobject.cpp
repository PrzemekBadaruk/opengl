#include "gameobject.h"

GameObject::GameObject() : m_material(new Material()), m_model(1.0f), m_rotationX(0.0f), m_rotationY(0.0f), m_rotationZ(0.0f), m_size(1.0f)
{

}

GameObject::~GameObject()
{
	delete m_material;
}

void GameObject::addMaterial(const Material* material)
{
	*m_material = *material;
}
Material* GameObject::material() const
{
	return m_material;
}

void GameObject::setPos(glm::vec3 newPos)
{
	m_pos = newPos;
}

glm::vec3 GameObject::pos() const
{
	return glm::vec3();
}

void GameObject::moveBy(float dx, float dy, float dz)
{
	glm::vec3 oldPos = pos();
	glm::vec3 newPos = glm::vec3(oldPos.x + dx, oldPos.y + dy, oldPos.z + dz);
	setPos(newPos);
}

void GameObject::setSize(glm::vec3 newSize)
{
	m_size = newSize;
}

glm::vec3 GameObject::size() const
{
	return glm::vec3();
}

void GameObject::setRotationX(float rotation)
{
	m_rotationX = rotation;
}

void GameObject::addRotationX(float offset)
{
	float _rotationX = rotationX();
	setRotationX(_rotationX + offset);
}

void GameObject::setRotationY(float rotation)
{
	m_rotationY = rotation;
}

void GameObject::addRotationY(float offset)
{
	float _rotationY = rotationY();
	setRotationY(_rotationY + offset);
}

void GameObject::setRotationZ(float rotation)
{
	m_rotationZ = rotation;
}

void GameObject::addRotationZ(float offset)
{
	float _rotationZ = rotationZ();
	setRotationY(_rotationZ + offset);
}

float GameObject::rotationX() const
{
	return m_rotationX;
}

float GameObject::rotationY() const
{
	return m_rotationY;
}

float GameObject::rotationZ() const
{
	return m_rotationZ;
}

void GameObject::submitModel()
{
	m_model = glm::mat4(1.0f);
	m_model = glm::translate(m_model, m_pos);

	
	// first translate (transformations are: scale happens first, then rotation, and then final translation happens; reversed order
	m_model = glm::translate(m_model, glm::vec3(0.5f * m_size.x, 0.5f * m_size.y, 0.5f * m_size.z)); // move origin of rotation to center of quad
	m_model = glm::rotate(m_model, glm::radians(m_rotationX), glm::vec3(1.0f, 0.0f, 0.0f)); // then rotate X
	m_model = glm::rotate(m_model, glm::radians(m_rotationY), glm::vec3(0.0f, 1.0f, 0.0f)); // then rotate Y
	m_model = glm::rotate(m_model, glm::radians(m_rotationZ), glm::vec3(0.0f, 0.0f, 1.0f)); // then rotate Z
	m_model = glm::translate(m_model, glm::vec3(-0.5f * m_size.x, -0.5f * m_size.y, 0.0f)); // move origin back
	m_model = glm::scale(m_model, glm::vec3(m_size)); // last scale
}

glm::mat4 GameObject::model() const
{
	return m_model;
}
 