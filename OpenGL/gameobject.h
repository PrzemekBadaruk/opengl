#ifndef _GAMEOBJECT_H_
#define _GAMEOBJECT_H_
#include "material.h"
#include <glm/glm.hpp>

class GameObject
{
public:
	GameObject();
	~GameObject();
	
	void addMaterial(const Material* material);
	Material* material() const;

	void setPos(glm::vec3 newPos);
	glm::vec3 pos() const;

	void moveBy(float dx, float dy, float dz);

	void setSize(glm::vec3 newSize);
	glm::vec3 size() const;

	void setRotationX(float rotation);
	void addRotationX(float offset);

	void setRotationY(float rotation);
	void addRotationY(float offset);

	void setRotationZ(float rotation);
	void addRotationZ(float offset);
	
	float rotationX() const;
	float rotationY() const;
	float rotationZ() const;

	void submitModel();
	glm::mat4 model() const;
private:
	Material* m_material;
	glm::mat4 m_model;
	glm::vec3 m_pos;
	glm::vec3 m_size;
	float m_rotationX, m_rotationY, m_rotationZ;
};

#endif