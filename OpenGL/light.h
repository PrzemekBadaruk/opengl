#ifndef _LIGHT_H_
#define _LIGHT_H_
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

struct Light
{
	Light(glm::vec3 _ambient, glm::vec3 _diffuse, glm::vec3 _spectular) : ambient(_ambient), diffuse(_diffuse), spectular(_spectular) {}
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 spectular;
};


static const Light WHITE_LIGHT = Light(glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f), glm::vec3(1.0f, 1.0f, 1.0f));
static const Light RED_LIGHT = Light(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f));
static const Light GREEN_LIGHT = Light(glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
static const Light BLUE_LIGHT = Light(glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 1.0f));

#endif