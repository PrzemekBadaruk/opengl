#ifndef _SCENE_H_
#define _SCENE_H_
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
class GLFWwindow;
class Scene
{
public:
	int width, height;
	Scene() : width(0), height(0), m_animate(false) {}
	virtual ~Scene() {}
	void setDimensions(int w, int h)
	{
		width = w; height = h;
	}

	/*
		Load textures, initialize shaders, etc.
	*/
	virtual void initScene() = 0;

	/*
		This is called prior to every frame. Use this to update your anim
	*/
	virtual void update(float t) = 0;

	/*
		Draw your scene.
	*/
	virtual void render() = 0;
	/*
		Called when screen is resized
	*/
	virtual void resize(int, int) = 0;

	virtual void processInput(GLFWwindow* window, float deltaTime) = 0;
	virtual void processMouse(GLFWwindow* window, double xpos, double ypos) = 0;
	virtual void processScroll(GLFWwindow* window, double offset) = 0;
	
	void animate(bool value) { m_animate = value; }
	bool animating() { return m_animate; }

protected:
	bool m_animate;
	glm::mat4 model, view, projection;
};

#endif // !_SCENE_H_
