#include "scenephong.h"
#include "light.h"
#include "material.h"
#include <GLFW/glfw3.h>
#include <iostream>

ScenePhong::ScenePhong() 
    : Scene(), cube(1.0f), camera(glm::vec3(0.0f, 0.0f, 3.0f)), torus(0.7f, 0.3f, 30, 30), sphere(1.0f, 40, 40), angle(0.0f)
{

}

void ScenePhong::initScene()
{
    compileAndLinkShader();

    glEnable(GL_DEPTH_TEST);

    Light light = WHITE_LIGHT;
    //view = glm::lookAt(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    view = camera.GetViewMatrix();
    projection = glm::mat4(1.0f);
    glm::vec4 worldLight = glm::vec4(5.0f, 5.0f, 2.0f, 1.0f);
    prog.setUniform("Light.Diffuse", light.diffuse);
    prog.setUniform("Light.Position", view * worldLight);
    prog.setUniform("Light.Ambient", light.ambient);
    prog.setUniform("Light.Specular", light.spectular);
    

}

void ScenePhong::update(float t)
{
}

void ScenePhong::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    view = camera.GetViewMatrix();

    Material *dynamic_material = new Material();
    *dynamic_material = GOLD_MATERIAL;
    //cube
    model = glm::mat4(1.0f);
    model = glm::rotate(model, glm::radians(angle), glm::vec3(0.0f, 1.0f, 0.0f));
    model = glm::rotate(model, glm::radians(-35.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    model = glm::rotate(model, glm::radians(35.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    cube.setRotationX(0.0f);
    cube.setRotationY(0.0f);
    cube.setRotationZ(0.0f);
    cube.setSize(glm::vec3(1.0f, 1.0f, 1.0f));
    cube.submitModel();
    model = cube.model();
    setMatrices();
    cube.addMaterial(dynamic_material);
    prog.setUniform("Material.Diffuse", dynamic_material->diffuse);
    prog.setUniform("Material.Ambient", dynamic_material->ambient);
    prog.setUniform("Material.Specular", dynamic_material->specular);
    prog.setUniform("Material.Shininess", dynamic_material->shininess);
    cube.render();

    //torus    
    model = glm::mat4(1.0f);
    torus.setPos(glm::vec3(0.0f, 3.0f, 0.0f));
    torus.submitModel();
    model = torus.model();
    setMatrices();
    Material material = EMERALD_MATERIAL;
    prog.setUniform("Material.Diffuse", material.diffuse);
    prog.setUniform("Material.Ambient", material.ambient);
    prog.setUniform("Material.Specular", material.specular);
    prog.setUniform("Material.Shininess", material.shininess);
    torus.render();

    //sphere
    model = glm::mat4(1.0f);
    sphere.setPos(glm::vec3(-3.0f, 3.0f, 0.0f));
    sphere.submitModel();
    model = sphere.model();
    setMatrices();
    sphere.render();

    delete dynamic_material;
}

void ScenePhong::resize(int w, int h)
{
    glViewport(0, 0, w, h);
    width = w;
    height = h;
    projection = glm::perspective(glm::radians(70.0f), (float)w / h, 0.3f, 100.0f);
    //settings for camera
    camera.lastX = w / 2;
    camera.lastY = h / 2;
}

void ScenePhong::processInput(GLFWwindow* window, float deltaTime)
{
    camera.ProcessInput(window, deltaTime);
}

void ScenePhong::setMatrices()
{
    glm::mat4 mv = view * model;
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("NormalMatrix",
        glm::mat3(glm::vec3(mv[0]), glm::vec3(mv[1]), glm::vec3(mv[2])));
    prog.setUniform("MVP", projection * mv);
}

void ScenePhong::compileAndLinkShader()
{
    try {
        prog.compileShader("shader/phong.vert.glsl");
        prog.compileShader("shader/phong.frag.glsl");
        //prog.compileShader("shader/function.vert.glsl");
        //prog.compileShader("shader/function.frag.glsl");
        prog.link();
        prog.use();
    }
    catch (ShaderException& e) {
        std::cerr << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}

void ScenePhong::processMouse(GLFWwindow* window, double xpos, double ypos)
{
    if (camera.firstMouse)
    {
        camera.lastX = xpos;
        camera.lastY = ypos;
        camera.firstMouse = false;
    }

    float xoffset = xpos - camera.lastX;
    float yoffset = camera.lastY - ypos; // reversed since y-coordinates go from bottom to top

    camera.lastX = xpos;
    camera.lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

void ScenePhong::processScroll(GLFWwindow* window, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}

