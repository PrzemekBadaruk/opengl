#ifndef _SCENE_PHONG_H_
#define _SCENE_PHONG_H_

#include "scene.h"
#include "shader.h"
#include "camera.h"
#include "cube.h"
#include "torus.h"
#include "sphere.h"
//#include <glm/glm.hpp>


class ScenePhong : public Scene
{
public:
    ScenePhong();

    void initScene();
    void update(float t);
    void render();
    void resize(int, int);
    void processInput(GLFWwindow*, float);
    void processMouse(GLFWwindow* window, double xpos, double ypos);
    void processScroll(GLFWwindow* window, double offset);

    Camera getCamera()
    {
        return camera;
    }
private:
    Shader prog;
    Cube cube;
    Torus torus;
    Sphere sphere;
    float angle;
    
    Camera camera; // def pos => glm::vec3(0.0f, 0.0f, 3.0f)
    


    void setMatrices();
    void compileAndLinkShader();
};


#endif //  
