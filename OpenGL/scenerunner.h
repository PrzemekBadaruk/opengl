#ifndef _SCENERUNNER_H_
#define _SCENERUNNER_H_
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "scene.h"



#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <memory>

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{

}

class SceneRunner
{
public:
	SceneRunner(const std::string& windowTitle, int width = WIN_WIDTH, int height = WIN_HEIGHT, int samples = 0) 
		: debug(true), deltaTime(0.0f), lastFrame(0.0f), m_scene(nullptr)
	{
		if (!glfwInit()) exit(EXIT_FAILURE);

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);

		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
		if (debug)
		{
			glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
		}
		if (samples > 0)
		{
			glfwWindowHint(GLFW_SAMPLES, samples);
		}

		// Open the window
		window = glfwCreateWindow(WIN_WIDTH, WIN_HEIGHT, windowTitle.c_str(), NULL, NULL);
		if (!window)
		{
			std::cerr << "Unable to create OpenGL context." << std::endl;
		}
		glfwMakeContextCurrent(window);
		glfwGetFramebufferSize(window, &fbw, &fbh);
		glfwSetWindowUserPointer(window, this);
		glfwSetCursorPosCallback(window, [](GLFWwindow* window, double x, double y)
			{
				static_cast<SceneRunner*>(glfwGetWindowUserPointer(window))->mouse_callback(window, x, y);
			}
		);
		glfwSetScrollCallback(window, [](GLFWwindow* window, double x, double y) 
			{
				static_cast<SceneRunner*>(glfwGetWindowUserPointer(window))->scroll_callback(window, x, y);
			}
		);

		// tell GLFW to capture our mouse
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

		if (!gladLoadGL()) 
		{ 
			std::cerr << "Unagle to load gladGL" << std::endl;
			exit(-1); 
		}

		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	}

	int run(std::unique_ptr<Scene> scene)
	{
		mainLoop(window, std::move(scene));
		glfwTerminate();
		return EXIT_SUCCESS;
	}

	static std::string parseCLArgs(int argc, char** argv, std::map<std::string, std::string>& sceneData) {
		if (argc < 2) {
			printHelpInfo(argv[0], sceneData);
			exit(EXIT_FAILURE);
		}

		std::string recipeName = argv[1];
		auto it = sceneData.find(recipeName);
		if (it == sceneData.end()) {
			printf("Unknown recipe: %s\n\n", recipeName.c_str());
			printHelpInfo(argv[0], sceneData);
			exit(EXIT_FAILURE);
		}

		return recipeName;
	}
	void mouse_callback(GLFWwindow* window, double xpos, double ypos)
	{
		if (m_scene)
		{
			m_scene->processMouse(window, xpos, ypos);
		}
	}
	void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
	{
		if (m_scene)
		{
			m_scene->processScroll(window, yoffset);
		}
	}
private:
	GLFWwindow* window;
	int fbw, fbh;
	bool debug;
	float deltaTime, lastFrame;
	Scene* m_scene;
	void mainLoop(GLFWwindow* window, std::unique_ptr<Scene> scene)
	{
		m_scene = scene.get();
		m_scene->setDimensions(fbw, fbh);
		m_scene->initScene();
		m_scene->resize(fbw, fbh);

		while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE))
		{
			float currentFrame = glfwGetTime();
			deltaTime = currentFrame - lastFrame;
			lastFrame = currentFrame;


			m_scene->update(glfwGetTime());
			m_scene->render();
			m_scene->processInput(window, deltaTime);
			glfwSwapBuffers(window);
			glfwPollEvents();
			
			int state = glfwGetKey(window, GLFW_KEY_SPACE);
			if (state == GLFW_PRESS)
			{
				m_scene->animate(!m_scene->animating());
			}
		}
	}
	static void printHelpInfo(const char* exeFile, std::map<std::string, std::string>& sceneData)
	{
		printf("Usage: %s recipe-name\n\n", exeFile);
		printf("Recipe names: \n");
	}

};


#endif