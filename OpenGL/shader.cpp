#include "shader.h"

#include <sstream>
#include <sys/stat.h>
#include <vector>
#include <cstdio>

namespace ShaderTypeFileExtension
{
	std::map<std::string, ShaderInfo::ShaderType> extensions = 
	{
		{".vs", ShaderInfo::VERTEX},
		{".vert", ShaderInfo::VERTEX},
		{"_vert.glsl", ShaderInfo::VERTEX},
		{".vert.glsl", ShaderInfo::VERTEX},

		{".fs", ShaderInfo::FRAGMENT},
		{".frag", ShaderInfo::FRAGMENT},
		{".frag.glsl", ShaderInfo::FRAGMENT},
		{"_frag.glsl", ShaderInfo::FRAGMENT},

		{".gs", ShaderInfo::GEOMETRY},
		{".geom", ShaderInfo::GEOMETRY},
		{".geom.glsl", ShaderInfo::GEOMETRY},
		{"_geom.glsl", ShaderInfo::GEOMETRY},

		{".tcs",  ShaderInfo::TESS_CONTROL},
		{".tcs.glsl",  ShaderInfo::TESS_CONTROL},
		
		{".tes",  ShaderInfo::TESS_EVALUATION},
		{".tes.glsl",  ShaderInfo::TESS_EVALUATION},

		{".cs",  ShaderInfo::COMPUTE},
		{".cs.glsl",  ShaderInfo::COMPUTE}
	};
	void printAllPossibleExtensions() {
		printf("POSSIBLE ALL SHADER FILE EXTENSIONS\n");
		printf("VERTEX => .vs/.vert/_vert.glsl/.vert.glsl\n");
		printf("FRAGMENT => .fs/.frag/_frag.glsl/.frag.glsl\n");
		printf("GEOMETRY => .gs/.geom/_geom.glsl/.geom.glsl\n");
		printf("TESS_CONTROL => .tcs/.tcs.glsl\n");
		printf("TESS_EVALUATION => .tes/.tes.glsl\n");
		printf("COMPUTE => .cs/cs.glsl\n");
	}
	std::string getAllPossibleExtensions() {
		std::string retValue = std::string("POSSIBLE ALL SHADER FILE EXTENSIONS\n") +
			std::string("VERTEX => .vs/.vert/_vert.glsl/.vert.glsl\n") +
			std::string("FRAGMENT => .fs/.frag/_frag.glsl/.frag.glsl\n") +
			std::string("GEOMETRY => .gs/.geom/_geom.glsl/.geom.glsl\n") +
			std::string("TESS_CONTROL => .tcs/.tcs.glsl\n ") +
			std::string("TESS_EVALUATION => .tes/.tes.glsl\n ") +
			std::string("COMPUTE => .cs/.cs.glsl\n ");
		return retValue;
	}
};

Shader::Shader()
{
}

Shader::Shader(const char* vertexPath, const char* fragmentPath)
{
    // 1. retrieve the vertex/fragment source code from filePath
    std::string vertexCode;
    std::string fragmentCode;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    // ensure ifstream objects can throw exceptions:
    vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        // open files
        vShaderFile.open(vertexPath);
        fShaderFile.open(fragmentPath);
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        // close file handlers
        vShaderFile.close();
        fShaderFile.close();
        // convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }
    const char* vShaderCode = vertexCode.c_str();
    const char* fShaderCode = fragmentCode.c_str();

    // 2. compile shaders
    unsigned int vertex, fragment;
    int success;
    char infoLog[512];

    // vertex Shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    // print compile errors if any
    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    };

    // similiar for Fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }


    // shader Program
    ID = glCreateProgram();
    glAttachShader(ID, vertex);
    glAttachShader(ID, fragment);
    glLinkProgram(ID);
    // print linking errors if any
    glGetProgramiv(ID, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(ID, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    // delete the shaders as they're linked into our program now and no longer necessary
    glDeleteShader(vertex);
    glDeleteShader(fragment);
}

Shader::~Shader()
{
	if (ID == 0) return;
	detachAndDeleteShaderObjects();
	glDeleteProgram(ID);
}

void Shader::compileShader(const char * fileName)
{
	// Check the file name's extension to determine the shader type
	std::string ext = getExtension(fileName);
	ShaderInfo::ShaderType type = ShaderInfo::VERTEX;
	auto it = ShaderTypeFileExtension::extensions.find(ext);
	if (it != ShaderTypeFileExtension::extensions.end()) {
		type = it->second;
	}
	else {
		std::string msg = "Unrecognized extension: " + ext + "\n";
		msg += ShaderTypeFileExtension::getAllPossibleExtensions();
		throw ShaderException(msg);
	}

	// Pass the discovered shader type along
	compileShader(fileName, type);
}

void Shader::compileShader(const char * fileName, ShaderInfo::ShaderType type)
{
	if (!fileExists(fileName))
	{
		std::string msg = std::string("Shader: ") + fileName + " not found.";
		throw ShaderException(msg);
	}
	if (ID <= 0)
	{
		ID = glCreateProgram();
		if (ID == 0)
		{
			throw ShaderException("Unable to create shader program.");
		}
	}
	std::ifstream inFile(fileName, std::ios::in);
	if (!inFile)
	{
		std::string msg = std::string("Unable to open: ") + fileName;
		throw ShaderException(msg);
	}
	std::stringstream code;
	code << inFile.rdbuf();
	inFile.close();
	compileShader(code.str(), type, fileName);
}

void Shader::compileShader(const std::string & source, ShaderInfo::ShaderType type, const char * fileName)
{
	if (ID <= 0)
	{
		ID = glCreateProgram();
		if (ID == 0)
		{
			throw ShaderException("Unable to create shader program.");
		}
	}

	GLuint shaderHandle = glCreateShader(type);

	const char *c_code = source.c_str();
	glShaderSource(shaderHandle, 1, &c_code, NULL);
	//Compile the shader
	glCompileShader(shaderHandle);

	//Check for errors
	int result;
	glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &result);
	if (GL_FALSE == result)
	{
		//Compile failed, get log
		std::string msg;
		if (fileName)
		{
			msg = std::string(fileName) + ": shader compilation failed\n";
		}
		else
		{
			msg = "Shader compilation failed.\n";
		}
		int length = 0;
		glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &length);
		if (length > 0)
		{
			std::string log(length, ' ');
			int written = 0;
			glGetShaderInfoLog(shaderHandle, length, &written, &log[0]);
			msg += log;
		}
		throw ShaderException(msg);
	}
	else
	{
		glAttachShader(ID, shaderHandle);
	}
}


void Shader::use()
{
	if (ID <= 0 || (!linked))
	{
		throw ShaderException("Shader has not been linked");
	}
	glUseProgram(ID);
}

void Shader::link()
{
	if (linked) return;
	if (ID <= 0) throw ShaderException("Program has not been compiled.");
	glLinkProgram(ID);
	int status = 0;
	std::string errString;
	glGetProgramiv(ID, GL_LINK_STATUS, &status);
	if (GL_FALSE == status)
	{
		int length = 0;
		glGetProgramiv(ID, GL_INFO_LOG_LENGTH, &length);
		errString += "Program link failed:\n";
		if (length > 0)
		{
			std::string log(length, ' ');
			int written = 0;
			glGetProgramInfoLog(ID, length, &written, &log[0]);
			errString += log;
		}
	}
	else
	{
		//findUniformLocations();
		linked = true;
	}
	
	detachAndDeleteShaderObjects();
	if (GL_FALSE == status) throw ShaderException(errString);
}

void Shader::setUniform(const char* name, float x, float y, float z)
{
    GLint loc = getUniformLocation(name);
    glUniform3f(loc, x, y, z);
}

void Shader::setUniform(const char* name, float x, float y, float z, float w)
{
    GLint loc = getUniformLocation(name);
    glUniform4f(loc, x, y, z, w);
}

void Shader::setUniform(const char* name, const glm::vec2& v)
{
    GLint loc = getUniformLocation(name);
    glUniform2f(loc, v.x, v.y);
}

void Shader::setUniform(const char* name, const glm::vec3& v)
{
    //overload func void Shader::setUniform(const char* name, float x, float y, float z)
    this->setUniform(name, v.x, v.y, v.z);
}

void Shader::setUniform(const char* name, const glm::vec4& v)
{
    //overload func void Shader::setUniform(const char* name, float x, float y, float z, float w)
    this->setUniform(name, v.x, v.y, v.z, v.w);
}

void Shader::setUniform(const char* name, const glm::mat4& m)
{
	GLint loc = getUniformLocation(name);
	glUniformMatrix4fv(loc, 1, GL_FALSE, &m[0][0]);
}

void Shader::setUniform(const char* name, const glm::mat3& m)
{
	GLint loc = getUniformLocation(name);
	glUniformMatrix3fv(loc, 1, GL_FALSE, &m[0][0]);
}

void Shader::setUniform(const char* name, float val)
{
	GLint loc = getUniformLocation(name);
	glUniform1f(loc, val);
}

void Shader::setUniform(const char* name, int val)
{
	GLint loc = getUniformLocation(name);
	glUniform1i(loc, val);
}

void Shader::setUniform(const char* name, bool val)
{
	GLint loc = getUniformLocation(name);
	glUniform1i(loc, (int)val);
}

void Shader::setUniform(const char* name, GLuint val)
{
	GLint loc = getUniformLocation(name);
	glUniform1ui(loc, val);
}

void Shader::setBool(const char* name, bool value)
{
	//overload func void Shader::setUniform(const char* name, bool val)
	this->setUniform(name, value);
}

void Shader::setInt(const char* name, int value)
{
	//overload func void Shader::setUniform(const char* name, int val)
	this->setUniform(name, value);
}

void Shader::setFloat(const char* name, float value)
{
	//overload func void Shader::setUniform(const char* name, float val)
	this->setUniform(name, value);
}

void Shader::setVec2(const char * name, const glm::vec2 & v)
{
	//overload func void Shader::setUniform(const char* name, const glm::vec2& v)
	this->setUniform(name, v);
}

void Shader::setVec3(const char * name, const glm::vec3 & v)
{
	//overload func void Shader::setUniform(const char* name, const glm::vec3& v)
	this->setUniform(name, v);
}

void Shader::setVec4(const char * name, const glm::vec4 & v)
{
	//overload func void Shader::setUniform(const char* name, const glm::vec4& v)
	this->setUniform(name, v);
}

void Shader::setMat3(const char * name, const glm::mat3 & m)
{
	//overload func void Shader::setUniform(const char* name, const glm::mat3& v)
	this->setUniform(name, m);
}

void Shader::setMat4(const char * name, const glm::mat4 & m)
{
	//overload func void Shader::setUniform(const char* name, const glm::mat4& v)
	this->setUniform(name, m);
}

void Shader::findUniformLocations()
{
	uniformLocations.clear();
	GLint numUniforms = 0;
	// For OpenGL 4.3 and above, use glGetProgramResource
	glGetProgramInterfaceiv(ID, GL_UNIFORM, GL_ACTIVE_RESOURCES, &numUniforms);

	GLenum properties[] = { GL_NAME_LENGTH, GL_TYPE, GL_LOCATION, GL_BLOCK_INDEX };

	for (GLint i = 0; i < numUniforms; ++i) {
		GLint results[4];
		glGetProgramResourceiv(ID, GL_UNIFORM, i, 4, properties, 4, NULL, results);

		if (results[3] != -1) continue;  // Skip uniforms in blocks
		GLint nameBufSize = results[0] + 1;
		char * name = new char[nameBufSize];
		glGetProgramResourceName(ID, GL_UNIFORM, i, nameBufSize, NULL, name);
		uniformLocations[name] = results[2];
		delete[] name;
	}
}

void Shader::printActiveUniforms()
{
	// For OpenGL 4.3 and above, use glGetProgramResource
	GLint numUniforms = 0;
	glGetProgramInterfaceiv(ID, GL_UNIFORM, GL_ACTIVE_RESOURCES, &numUniforms);

	GLenum properties[] = { GL_NAME_LENGTH, GL_TYPE, GL_LOCATION, GL_BLOCK_INDEX };

	printf("Active uniforms:\n");
	for (int i = 0; i < numUniforms; ++i) {
		GLint results[4];
		glGetProgramResourceiv(ID, GL_UNIFORM, i, 4, properties, 4, NULL, results);

		if (results[3] != -1) continue;  // Skip uniforms in blocks
		GLint nameBufSize = results[0] + 1;
		char * name = new char[nameBufSize];
		glGetProgramResourceName(ID, GL_UNIFORM, i, nameBufSize, NULL, name);
		printf("%-5d %s (%s)\n", results[2], name, getTypeString(results[1]));
		delete[] name;
	}
}

void Shader::printActiveUniformBlocks()
{
	GLint numBlocks = 0;

	glGetProgramInterfaceiv(ID, GL_UNIFORM_BLOCK, GL_ACTIVE_RESOURCES, &numBlocks);
	GLenum blockProps[] = { GL_NUM_ACTIVE_VARIABLES, GL_NAME_LENGTH };
	GLenum blockIndex[] = { GL_ACTIVE_VARIABLES };
	GLenum props[] = { GL_NAME_LENGTH, GL_TYPE, GL_BLOCK_INDEX };

	for (int block = 0; block < numBlocks; ++block) {
		GLint blockInfo[2];
		glGetProgramResourceiv(ID, GL_UNIFORM_BLOCK, block, 2, blockProps, 2, NULL, blockInfo);
		GLint numUnis = blockInfo[0];

		char * blockName = new char[blockInfo[1] + 1];
		glGetProgramResourceName(ID, GL_UNIFORM_BLOCK, block, blockInfo[1] + 1, NULL, blockName);
		printf("Uniform block \"%s\":\n", blockName);
		delete[] blockName;

		GLint * unifIndexes = new GLint[numUnis];
		glGetProgramResourceiv(ID, GL_UNIFORM_BLOCK, block, 1, blockIndex, numUnis, NULL, unifIndexes);

		for (int unif = 0; unif < numUnis; ++unif) {
			GLint uniIndex = unifIndexes[unif];
			GLint results[3];
			glGetProgramResourceiv(ID, GL_UNIFORM, uniIndex, 3, props, 3, NULL, results);

			GLint nameBufSize = results[0] + 1;
			char * name = new char[nameBufSize];
			glGetProgramResourceName(ID, GL_UNIFORM, uniIndex, nameBufSize, NULL, name);
			printf("    %s (%s)\n", name, getTypeString(results[1]));
			delete[] name;
		}

		delete[] unifIndexes;
	}
}

void Shader::printActiveAttribs()
{
	GLint numAttribs;
	glGetProgramInterfaceiv(ID, GL_PROGRAM_INPUT, GL_ACTIVE_RESOURCES, &numAttribs);

	GLenum properties[] = { GL_NAME_LENGTH, GL_TYPE, GL_LOCATION };

	printf("Active attributes:\n");
	for (int i = 0; i < numAttribs; ++i) {
		GLint results[3];
		glGetProgramResourceiv(ID, GL_PROGRAM_INPUT, i, 3, properties, 3, NULL, results);

		GLint nameBufSize = results[0] + 1;
		char * name = new char[nameBufSize];
		glGetProgramResourceName(ID, GL_PROGRAM_INPUT, i, nameBufSize, NULL, name);
		printf("%-5d %s (%s)\n", results[2], name, getTypeString(results[1]));
		delete[] name;
	}
}

const char * Shader::getTypeString(GLenum type)
{
	switch (type)
	{
		case GL_BOOL:
			return "bool";
		case GL_UNSIGNED_INT:
			return "unsigned int";
		case GL_INT:
			return "int";
		case GL_FLOAT:
			return "float";
		case GL_FLOAT_VEC2:
			return "vec2";
		case GL_FLOAT_VEC3:
			return "vec3";
		case GL_FLOAT_VEC4:
			return "vec4";
		case GL_FLOAT_MAT2:
			return "mat2";
		case GL_FLOAT_MAT3:
			return "mat3";
		case GL_FLOAT_MAT4:
			return "mat4";
		default:
			return "unknown";
	}
}

std::string Shader::getExtension(const char * name)
{
	std::string nameStr = name;
	size_t dotLoc = nameStr.find_last_of('.');
	//return text after last dot, otherwise return empty string ""
	//if text end with ".glsl" return text before that phrase to last other dot
	if (dotLoc != std::string::npos)
	{
		std::string ext = nameStr.substr(dotLoc);
		if (ext == ".glsl")
		{
			size_t loc = nameStr.find_last_of('.', dotLoc - 1);
			if (loc == std::string::npos)
			{
				loc = nameStr.find_last_of('_', dotLoc - 1);
			}
			if (loc != std::string::npos)
			{
				return nameStr.substr(loc);
			}
		}
		else
		{
			return ext;
		}
	}
	return "";
}

bool Shader::fileExists(const char * fileName)
{
	struct stat info;
	int ret = -1;

	ret = stat(fileName, &info);
	return 0 == ret;
}

void Shader::detachAndDeleteShaderObjects()
{
	GLint numShaders = 0;
	glGetProgramiv(ID, GL_ATTACHED_SHADERS, &numShaders);
	std::vector<GLuint> shaderNames(numShaders);
	glGetAttachedShaders(ID, numShaders, NULL, shaderNames.data());
	for (GLuint shader : shaderNames)
	{
		glDetachShader(ID, shader);
		glDeleteShader(shader);
	}
}


int Shader::getUniformLocation(const char* name)
{
    auto pos = uniformLocations.find(name);
    //return -1 if name uniform deosn't exist
    if (pos == uniformLocations.end()) {
        GLint loc = glGetUniformLocation(ID, name);
        uniformLocations[name] = loc;
        return loc;
    }

    //return pos which based on 
    return pos->second;
}

ShaderException::ShaderException(const std::string & message) : std::runtime_error(message)
{

}
