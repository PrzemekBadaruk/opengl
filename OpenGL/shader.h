#ifndef _SHADER_H_
#define SHADER_H_

#include <glad/glad.h> // include glad to get all the required OpenGL headers
#include <glm/glm.hpp>

#include <iostream>
#include <map>
#include <fstream>
#include <stdexcept>
#include <string>
#include <sstream>


namespace ShaderInfo
{
	enum ShaderType {
		VERTEX = GL_VERTEX_SHADER,
		FRAGMENT = GL_FRAGMENT_SHADER,
		GEOMETRY = GL_GEOMETRY_SHADER,
		TESS_CONTROL = GL_TESS_CONTROL_SHADER,
		TESS_EVALUATION = GL_TESS_EVALUATION_SHADER,
		COMPUTE = GL_COMPUTE_SHADER
	};
};

class ShaderException : public std::runtime_error
{
public:
	ShaderException(const std::string& message);
};

class Shader
{
public:
	// constructor reads and builds the shader
	Shader();
	Shader(const char* vertexPath, const char* fragmentPath);
	Shader(const Shader&) = delete;
	Shader& operator=(const Shader&) = delete;

	~Shader();
	void compileShader(const char* fileName);
	void compileShader(const char* fileName, ShaderInfo::ShaderType type);
	void compileShader(const std::string &source, ShaderInfo::ShaderType type, const char* fileName);

    // the program ID
    unsigned int ID;

    
    // use/activate the shader
    void use();
	void link();
    // utility uniform functions
    void setUniform(const char* name, float x, float y, float z);
    void setUniform(const char* name, float x, float y, float z, float w);
    void setUniform(const char* name, const glm::vec2& v);
    void setUniform(const char* name, const glm::vec3& v);
    void setUniform(const char* name, const glm::vec4& v);
    void setUniform(const char* name, const glm::mat4& m);
    void setUniform(const char* name, const glm::mat3& m);
    void setUniform(const char* name, float val);
    void setUniform(const char* name, int val);
    void setUniform(const char* name, bool val);
    void setUniform(const char* name, GLuint val);

    void setBool(const char* name, bool value);
    void setInt(const char* name, int value);
    void setFloat(const char* name, float value);
    void setVec2(const char* name, const glm::vec2& v);
	void setVec3(const char* name, const glm::vec3& v);
	void setVec4(const char* name, const glm::vec4& v);
	void setMat3(const char* name, const glm::mat3& m);
	void setMat4(const char* name, const glm::mat4& m);

	//debug func
	void findUniformLocations();
	void printActiveUniforms();
	void printActiveUniformBlocks();
	void printActiveAttribs();

	//utility func
	const char *getTypeString(GLenum type);
	std::string getExtension(const char* name);
	bool fileExists(const char* fileName);
private:
	void detachAndDeleteShaderObjects();

    int getUniformLocation(const char* name);
    std::map<std::string, int> uniformLocations;
	bool linked;
};

#endif