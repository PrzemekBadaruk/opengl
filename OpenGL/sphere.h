#ifndef _SPHERE_H_
#define _SPHERE_H_
#include "trianglemesh.h"
#include "gameobject.h"

class Sphere : public TriangleMesh, public GameObject
{
public:
    Sphere(float rad, GLuint sl, GLuint st);
};

#endif