#include "texture2D.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include <iostream>
Texture2D::Texture2D() : mTexture(0)
{
}

Texture2D::~Texture2D()
{
}

bool Texture2D::loadTexture(const string& filename, bool generateMipMaps)
{
    int width, height, components;
    unsigned char* imageData = stbi_load(filename.c_str(), &width, &height, &components, 0);
    if (imageData == NULL)
    {
        std::cerr << "Error loading texture '" << filename << "'" << std::endl;
        return false;
    }
    else
    {
        std::cout << "Success loading texture '" << filename << "'" << std::endl;
    }

    glGenTextures(1, &mTexture);
    glBindTexture(GL_TEXTURE_2D, mTexture);

    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glTexParameteri(GL_TEXTURE, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageData);
    
    if (generateMipMaps)
    {
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    stbi_image_free(imageData);
    glBindTexture(GL_TEXTURE_2D, 0);

    return true;
}

void Texture2D::bind(GLuint texUnit)
{
    glActiveTexture(GL_TEXTURE0 + texUnit);
    glBindTexture(GL_TEXTURE_2D, mTexture);
}
