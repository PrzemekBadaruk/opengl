#ifndef _TEXTURE2D_H_
#define _TEXTURE2D_H_
#include <glad/glad.h>
#include <string>

using std::string;

class Texture2D
{
public:
	Texture2D();
	virtual ~Texture2D();

	bool loadTexture(const string& filename, bool generateMipMaps = true);
	void bind(GLuint texUnit = 0);

private:
	GLuint mTexture;
};

#endif 