#ifndef _TORUS_H_
#define _TORUS_H_
#include "drawable.h"
#include "trianglemesh.h"
#include "gameobject.h"

class Torus : public TriangleMesh, public GameObject
{
public:
    Torus(GLfloat outerRadius, GLfloat innerRadius, GLuint nsides, GLuint nrings);
};

#endif